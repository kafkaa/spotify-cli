## spotify-cli 

Access and manage an authorized user's account vis the Spotify Web-API.

## Overview

Access and manage your spotify user account from the command line. 

## Requirements

+ [Python](https://python.org/)
+ [pipenv](https://pipenv.pypa.io/en/latest/)

#### Or

+ [docker](https://docker.org)

## Getting Started

#### Use Docker

    docker pull thepromethean/spotify:slim
    docker run -it --name spotify-api --rm thepromethean/spotify:slim 

#### Manual Install

    git clone https://gitlab.com/kafkaa/spotify-cli.git
    cd spotify-cli
    pipenv install
    echo -e "#!$(pwd)/.venv/bin/python3\n$(cat main.py)" > main.py
    chmod u+x main.py 
    cd /usr/local/bin
    ln -s /full/path/to/main.py spotify

## Usage

    spotify [command] [subcommand] [--options]

    COMMANDS:

    user:  account authorization and managment
    get:   fetch data from the api
    set:   set application paramters
    track: perform operations on track data

    SUBCOMMANDS:

    user activate  activate the app and authorize endpoints

    get info:      return information about the currently playing track
    fet playlist   displays current active playlist
    get playlists: retrieve lis of playlists and sets active playlist

    set playlist:  activate a playlist for application operations

    track like:    add the currently playing track to the active playlist
    track dislike: remove currently playing track from the active playlist

### authorization

The first time you run the client, you'll be required to 
authorize the app. Authorizing the app is done with the 
following command:

    spotify user activate --conf path/to/spot.auth

The spot.auth file is a json formatted configuration file containing 
the necessary authorization data. An example can be found in .access/spot.auth 

AUTH FILE:

    client:         the client id issued by Spotify for your app

    state:          csrf token string used as a url parameter to
                    prevent cross site forgery attacks. It is
                    optional and can be left out if desired.

    redirect_uri:   valid url to a domain you control used for
                    redirection. this is mandatory. To make this 
                    process easier, I have created an api endpoint
                    that extracts the authcode and presents it in
                    a webpage. If you choose not to use this redirect
                    url you will need to extract the authcode from the 
                    redirect url spotify produces after authentication.

    scope:          a list of the domains within the spotify api
                    this app will have permission to access. The
                    endpoints contained in the auth file are neccessary 
                    for all of the commands to work. This app is 
                    organized to be scalable, so adding new functionality 
                    is as easy as adding new command files and the scopes 
                    needed to accomdate them to the spot.auth file.

Runnning the command with the appropiate config file will launch
the default system browser, prompting spotify user login and 
account authorization of the app. Once authorization is granted,
the user is redirected to a webpage displaying the app's 
authorization code.

Copy the authcode and paste it into the command prompt. The app
will automatically use the authcode to request an access token
from the  Spotify API.  An access token is required to be in the
url parameters in order to make successful calls to the api.

This activation only needs be executed once because once an
access token is granted, it can be refreshed indefinitely.

The application scope can be modified to accommodate other
endpoints by adding the new scopes to the scope list in the
spot.auth file. However, new commands must be written accomodate
the new endpoints and activation process must be performed again
each time the scope is altered to grant the app the appropiate
permissions.

### comprehensive documentation of each command:

        spotify command --docs

## Issues


## Reference

+ [Spotify Developer API](https://developer.spotify.com/documentation/web-api/)

## License

+ [Apache](http://opensource.org/licenses/Apache-2.0)
