"""spotify-cli 1.2.0 [2021-10-27]

COMMAND:
    status

USAGE:
    spotify status help     : displays comprehensive documentation
    spotify status start    : start updating to tmux status bar (if configured and not running)
    spotify status stop     : stops updating to tmux status bar (if configured and running)

more info @https://tao-of-tmux.readthedocs.io/en/latest/manuscript/09-status-bar.html
"""
import os
import sys
import shlex
import subprocess as sp

from src.utils import fileops
from src.utils import setopts

from src.utils.process_manager import _reset_status
from src.utils.process_manager import _stop_status_bar
from src.utils.process_manager import _launch_status_bar

this = sys.modules[__name__]


class status(setopts.SetOpts):
    """spotify-cli 1.1.0 [2021-10-15]

STATUS:
    START:

    start the displaying current track data in the TMUX status bar if
    it is configured to do so.

    it's executed using the following syntax:

    spotify status start

    STOP:

    stop displaying current track data in the TMUX status bar if
    it is runnning and configured to do so.

    it's executed using the following syntax:

    spotify status stop

    HELP:

    displays this documentation
    it's executed using the following syntax:

    spotify create help

CONFIGURING THE TMUX STATUS BAR TO DISPLAY DATA FROM SPOTIFY:

more info @https://tao-of-tmux.readthedocs.io/en/latest/manuscript/09-status-bar.html
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        if self.options.cmd == 'start':
            _launch_status_bar()

        elif self.options.cmd == 'stop':
            _stop_status_bar()
            _reset_status()
