"""spotify-cli 1.2.0 [2021-10-27]

COMMAND:
    transport

USAGE:
    spotify transport help          get comprehensive documentation
    spotify transport play [--uri]  start playback on active device
    spotify transport pause         pause playback on active device
    spotify transport next          play next track in playlist
    spotify transport prev          play previous track n playlist


more info @https://developer.spotify.com/documentation/web-api/
"""
import sys
import json
from importlib import import_module

from src.utils import fileops
from src.utils import setopts
from src.api.client import APIClient
from src.api.endpoints import endpoints
from src.api.operations.transport import device


client = APIClient()
this = sys.modules[__name__]


class transport(setopts.SetOpts):
    """spotify-cli 1.2.0 [2021-10-27]

TRANSPORT:
    spotify player transport controls.

    PLAY/RESUME:
    play or resume playing the spotify transport. Optional argument can be
    can be added to select track, album or playlist.
    it's executed using the following syntax:

        spotify transport play --uri $URI --device  $DEVICE-ID

    PAUSE:
    stop the spotify transport if it is playing.
    it's executed using the following syntax:

        spotify transport stop


    NEXT:
    go to the next track in current album or playlist
    it's executed using the following syntax:

        spotify transport next

    PREV:
    go to the previous track in current album or playlist
    it's executed using the following syntax:

        spotify transport prev


    HELP
    displays this documentation

    it's executed using the following syntax:

        spotify get help

more information @https://developer.spotify.com/documentation/web-api/
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        url = getattr(endpoints.player, self.options.cmd).geturl()
        operation = str(fileops.operations / type(self).__name__)
        process = getattr(import_module(operation.replace('/', '.')), self.options.cmd)
        data = json.dumps(dict(context_uri=self.options.uri)) if 'uri' in self.options else None
        request = client.put if process.__name__ in ('play', 'pause') else client.post
        client.call(url, method=request, data=data, proc=process)
