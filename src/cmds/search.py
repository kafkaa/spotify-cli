"""spotify-cli 1.2.0 [2021-10-27]

COMMAND:
    search

USAGE:
    spotify search help        get comprehensive documentation
    spotify search track       get catalog information about tracks
    spotify search album       get catalog information about albums
    spotify search artist      get catalog information about artists
    spotify search playlist    get catalog information about playlists
    spotify search show        get catalog information about shows
    spotify search episode     get catalog information episodes
    spotify search all         get catalog information on all types 


more info @https://developer.spotify.com/documentation/web-api/
"""
import sys
from importlib import import_module

from src.utils import fileops
from src.utils import setopts
from src.api.client import APIClient
from src.api.endpoints import endpoints


client = APIClient()
this = sys.modules[__name__]


class search(setopts.SetOpts):
    """spotify-cli 1.2.0 [2021-10-27]

SEARCH:
    search returns the top 20 query results.

    TRACK:
    get spotify catalog information about tracks that match a keyword query.
    it's executed using the following syntax:

        spotify search track $KEYWORD

    ALBUM:
    get spotify catalog information about albums that match a keyword query.
    it's executed using the following syntax:

        spotify search album $KEYWORD


    ARTIST:
    get spotify catalog information about artists that match a keyword query.
    it's executed using the following syntax:

        spotify search artist $KEYWORD


    PLAYLIST:
    get spotify catalog information about playlists that match a keyword query.
    it's executed using the following syntax:

        spotify search playlist $KEYWORD


    SHOW:
    get spotify catalog information about shows that match a keyword query.
    it's executed using the following syntax:

        spotify search show $KEYWORD


    EPISODE:
    get spotify catalog information about episodes that match a keyword query.
    it's executed using the following syntax:

        spotify search episode $KEYWORD


    HELP
    displays this documentation

    it's executed using the following syntax:

        spotify get help

more information @https://developer.spotify.com/documentation/web-api/
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        url = endpoints.search.geturl()
        op = str(fileops.operations / type(self).__name__)
        process = getattr(import_module(op.replace('/', '.')), self.options.cmd)
        params = dict(q=self.options.keywords, type=self.options.cmd)
        client.call(url, method=client.get, params=params, proc=process)
