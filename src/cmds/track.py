"""spotify-cli 1.2.0 [2021-10-27]

COMMAND:
    track

USAGE:
    spotify track -e
    spotify track help 
    spotify track like
    spotify track dislike

OPTIONS:
    -e        displays the api endpoint url
    help      display comprehensive documentation
    like      add the currently playing track to the active playlist
    dislike   remove the currently playing track from the active playlist

more info @https://developer.spotify.com/documentation/web-api
"""
import sys
from importlib import import_module

from src.utils import fileops
from src.utils import setopts
from src.api.client import APIClient
from src.api.endpoints import endpoints

client = APIClient()
this = sys.modules[__name__]


class track(setopts.SetOpts):
    """spotify-cli 1.2.0 [2021-10-27]

TRACK:
    LIKE:
    add currently playing track to current active playlist

    the 'like' command adds the currently playing track to the active
    playlist. It's executed using the following syntax:

        spotify like

    'like' can be executed with the following options:

        spotify like -e: returns the endpoint url used in the query

    DISLIKE:
    removes the currently playing track from current playlist

    the 'dislike' command removes the currently playing track from the active
    playlist if it is contained therein

    'dislike' is executed using the following syntax:

        spotify dislike

    'dislike' can be executed with the following options:

        spotify dislike -e: returns the endpoint url used in the query

    HELP:
    show this documentation and exit

more info @https://developer.spotify.com/documentation/web-api
"""
    info = endpoints.track.info.geturl()
    plist = fileops.read(fileops.plist).strip()
    cache = fileops.cache_path / f'{plist}.plist'

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        if self.options.e:
            print(self.info)
            sys.exit()

        op = str(fileops.operations / type(self).__name__)
        process = getattr(import_module(op.replace('/', '.')), self.options.cmd)
        args = dict(cache=self.cache, playlist=fileops.read(fileops.plist))
        client.call(self.info, method=client.get, proc=process, args=args)
