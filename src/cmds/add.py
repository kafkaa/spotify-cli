"""spotify-cli 1.2.0 [2021-10-27]

COMMAND:
    add

USAGE:
    spotify add help          displays comprehensive documentation
    spotify add track         adds a track to a playlist
    spotify add album         adds an album to a playlist
    spotify add playlist      adds a playlist to another playlist

more info @https://developer.spotify.com/documentation/web-api/
"""
import sys
import json
import src.api.operations as ops

from src.utils import fileops
from src.utils import setopts
from src.api.client import APIClient
from src.api.endpoints import endpoints


client = APIClient()
this = sys.modules[__name__]


class add(setopts.SetOpts):
    """spotify-cli 1.1.0 [2020-03-21]

ADD:
    TRACK:

    add specified track to specified playlist
    it's executed using the following syntax:

    spotify add track $TRACK-URI $TARGET-PLAYLIST-ID:

    ALBUM:

    add specified album to specified playlist
    it's executed using the following syntax:

    spotify add album $ALBUM-ID $TARGET-PLAYLIST-ID:

    PLAYLIST:

    adds a spotify playlist to another spotify playlist
    it's executed using the following syntax:

    spotify add album $PLAYLIST-ID $TARGET-PLAYLIST-ID:

    HELP:

    displays this documentation
    it's executed using the following syntax:

    spotify add help

more information @https://developer.spotify.com/documentation/web-api/
"""
    ep = endpoints.playlist.tracks.geturl()

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        if self.options.cmd == 'track':
            uri = self.options.track_uri
            pid = self.options.plist_id
            args = dict(track=uri, playlist=pid)
            client.call(self.ep.format(pid), method=client.post, params={"uris": [uri]}, proc=ops.add.track, args=args)

        if self.options.cmd == 'album':
            aid = self.options.album_id
            pid = self.options.plist_id
            url = self.ep.format(pid)

            cache = fileops.cache_path / f'{aid}.album'
            album = endpoints.album.tracks.geturl().format(aid)
            songs = fileops.collect(cache, client, album, callback=ops.get.album, args=dict(file=cache, verbose=False))

            payload = json.dumps({"uris": songs})
            args = dict(album_id=aid, plist_id=pid)
            client.call(url, method=client.post, data=payload, proc=ops.add.album, args=args)

        if self.options.cmd == 'playlist':  # Not Working (works in broswer)
            pid = self.options.plist_id
            tpid = self.options.target_plist_id
            url = self.ep.format(pid)

            cache = fileops.cache_path / f'{pid}.plist'
            plist = endpoints.playlist.info.geturl() + pid
            songs = fileops.collect(cache, client, plist, callback=ops.get.playlist, args=dict(file=cache, entire=True, verbose=False))

            payload = json.dumps({"uris": songs})
            args = dict(plist_id=pid, target_plist_id=tpid)
            client.call(url, method=client.post, data=payload, proc=ops.add.playlist, args=args)
