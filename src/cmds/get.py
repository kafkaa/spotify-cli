"""spotify-cli 1.2.0 [2021-10-27]

COMMAND:
    get

USAGE:
    spotify get help                  displays comprehensive documentation
    spotify get info [-e]             query the api for info on the current track
    spotify get playlists             displays all user playlists
    spotify get playlist [plist-id]   fetches data for the specified playlist
    spotify get active-playlist-id    displays current active playlist
    spotify get username              displays current active username

OPTIONS:
    get info -e                       displays the api url endpoint
    get playlists -e                  displays the api url endpoint

more info @https://developer.spotify.com/documentation/web-api/
"""
import os
import sys
import src.api.operations as ops

from src.utils import fileops
from src.utils import setopts
from src.api.client import APIClient
from src.api.endpoints import endpoints


client = APIClient()
this = sys.modules[__name__]


class get(setopts.SetOpts):
    """spotify-cli 1.2.0 [2021-10-27]

GET:
    INFO:
    query the api for currently playing track data

    the 'info' command fetches artist, album and track information for
     the currently playing track. 

    it's executed using the following syntax:

        spotify get info:

        Track:  John McLaughlin      spotify:track:2bGs2tuqoThglpGFBWmXoz
        Album:     Bitches Brew      spotify:album:3Q0zkOZEOC855ErOOJ1AdO
        Artist:     Miles Davis      spotify:artist:0kbYTNQb4Pb1rPbbaF0pT4

    info can be executed with the following options:


    PLAYLISTS:
    query the api for a list of user playlists

    the 'playlists' command fetches a list of all of the current 
    user's playlists; returns each playlist title and its corresponding 
    uri.

    it's executed using the following syntax:

        spotify get playlists

            Drum and Bass           2XJXLXLf654mJQ2L7XjSOp
            Persian Santoor         3zjuehQMXE7mnhWrEWTQRu
            Hindustani-Classical    1OGlW1EMvfzcbqEnbbZ9eK
            Classic Dubstep         1nhohkpx5Qh0O4RYrkThMG

    PLAYLIST:
    get the information for a specified public playlist

    the playlist command queries the api to acquire information on the 
    specified playlist. 

    it's executed using the following syntax:

        spotify get playlist $playlist-id

    'playlist' can be executed with the following options:

        spotify get playlists --all    : get every track in the playlist (uses multiple queries)
        spotify get playlists --silent : supresses output; save track uris to tmp file
        spotify get playlists --header : get playlist metadat formatted as a header in the output

    ACTIVE-PLAYLIST-ID:
    get the id for the currently active user playlist

    fetches the playlist id for the current active playlist. Tracks are
    added to this playlist when they are liked via this interface. This
    playlist can be changed by using the spotify set active-playlist command.

    it's executed using the following syntax:

        spotify get active-playlist-id

            3ubPDy4WlKcCxenW7D8qNo

    USERNAME:
    get the username for the current active user

    it's executed using the following syntax:

        spotify get username

    HELP
    displays this documentation

    it's executed using the following syntax:

        spotify get help

more information @https://developer.spotify.com/documentation/web-api/
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        if self.options.cmd == 'info':
            url = endpoints.track.info.geturl()
            if self.options.proxy:
                request = client.prepare_request(url)
                client.call(client.PROXY, method=client.get, params={'d': request.url}, proc=ops.get.info)

            else:
                client.call(url, method=client.get, proc=ops.get.info)

        if self.options.cmd == 'devices':
            url = endpoints.player.devices.geturl()
            client.call(url, method=client.get, proc=ops.get.devices)

        if self.options.cmd == 'recent':
            url = endpoints.track.recent.geturl()
            client.call(url, method=client.get, proc=ops.get.recent)

        elif self.options.cmd == 'playlists':
            url = endpoints.playlist.user.geturl()
            client.call(url, method=client.get, proc=ops.get.playlists)

        elif self.options.cmd == 'playlist':
            pid = self.options.plist_id
            cache = fileops.cache_path / f'{pid}.plist'
            output = True if self.options.silent is False else False

            url = endpoints.playlist.info.geturl() + pid
            args = dict(file=cache, entire=self.options.all, header=self.options.add_header, verbose=output)
            client.call(url, method=client.get, proc=ops.get.playlist, args=args)

        elif self.options.cmd == 'album':
            aid = self.options.album_id
            cache = fileops.cache_path / f'{aid}.album'
            output = True if self.options.silent is False else False

            url = endpoints.album.tracks.geturl()
            args = dict(file=cache, header=self.options.add_header, album=aid, verbose=output)
            client.call(url.format(aid), method=client.get, proc=ops.get.album, args=args)

        elif self.options.cmd == 'active-playlist-id':
            print(fileops.read(fileops.plist).strip())
            sys.exit()

        elif self.options.cmd == 'username':
            print(fileops.read(fileops.user).strip())
            sys.exit()

        elif self.options.cmd == 'status-pid':
            if os.path.isfile(fileops.status_pid):
                print(fileops.read(fileops.status_pid).strip())
                sys.exit()
            sys.exit('status bar process is not running')
