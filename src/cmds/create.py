"""spotify-cli 1.2.0 [2021-10-27]

COMMAND:
    add 

USAGE: 
    spotify create help                 displays comprehensive documentation
    spotify create playlist -e          show the endpoit used to query the api
    spotify create playlist -n $NAME    cretes a new, empty spotify playlist

more info @https://developer.spotify.com/documentation/web-api/
"""
import sys
import json
import src.api.operations as ops

from src.utils import fileops
from src.utils import setopts
from src.api.client import APIClient
from src.api.endpoints import endpoints


client = APIClient()
this = sys.modules[__name__]


class create(setopts.SetOpts):
    """spotify-cli 1.1.0 [2021-10-15]

CREATE:
    PLAYLIST:

    create a new empty spotify playist
    it's executed using the following syntax:

    spotify create playlist -n $NAME

    HELP:

    displays this documentation
    it's executed using the following syntax:

    spotify create help

more information @https://developer.spotify.com/documentation/web-api/
"""
    user = fileops.read(fileops.user).strip()
    ep = endpoints.playlist.create.geturl().format(user)

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        if self.options.e and self.options.n:
            sys.exit("ERROR: The -e and -n flags are mutually exclusive. Please use one or the other.")

        if self.options.e:
            print(self.ep)

        if self.options.n:
            client.call(self.ep, method=client.post, data=json.dumps({"name": self.options.n}), proc=ops.create.playlist)
