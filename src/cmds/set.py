"""spotify-cli 1.2.0 [2021-10-27]

COMMAND:
    set (set app parameters)

USAGE:
    spotify set help                    : shows this documentation
    spotify set status                  : caches the location of tmux status bar file
    spotify set username [user-id]      : sets active spotify username
    spotify set playlist [playlist-id]  : sets the active playlist for liking and not liking tracks

more info @https://developer.spotify.com/documentation/web-api
"""
import sys
from src.utils import fileops
from src.utils import setopts

this = sys.modules[__name__]


class set(setopts.SetOpts):
    """spotify-cli 1.2.0 [2021-10-27]

SET:
    PLAYLIST:

    set the current active playlist

    the 'set playlist' command sets the active playlist to the playlists 
    specified by the playlist-id argument. This is the playlist that 
    is operated on whenever calls to the API are made.

    it's executed using the following syntax:

        spotify set playlist 1shohppx7Qh0O4VYrkThGM

    STATUS:

    cache the location of the file read by the tmux status bar

    the 'set status' command sets the location of the file used by
    the tmux status bar for displaying content in the status bar if 
    it is configured.

    it's executed using the following syntax:

        spotify set status $DIRECTORY/PATH/FILE

    USERNAME:

    set the current username id

    the 'set username' command sets the current user id. This your account 
    display name and it is used for making post requests to the current 
    user's spotify account.

    it's executed using the following syntax:

        spotify set username "your-username"

    HELP

    displays this documentation

    it's executed using the following syntax:

        spotify get help

more info @https://developer.spotify.com/documentation/web-api
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        if self.options.cmd == 'playlist':
            playlist = self.options.plist_id
            fileops.write(playlist, fileops.plist)
            print(f'playlist {playlist} has been activated.')
            sys.exit()

        elif self.options.cmd == 'username':
            user = self.options.userid
            fileops.write(user, fileops.user)
            print(f'current username set to {user}')
            sys.exit()

        elif self.options.cmd == 'status':
            status = self.options.status
            fileops.write(status, fileops.status_location)
            print(f'staus bar file location set: {status}')
            sys.exit()
