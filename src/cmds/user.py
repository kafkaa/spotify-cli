"""spotify-cli 1.2.0 [2021-10-27]

COMMAND: 
    user (account authorization and configuration)

USAGE:
    VIEW DOCUMENTATION:

        spotify user help

    AUTHORIZE THE APPLICATION TO ACCESS USER ACCOUNT:

        spotify user activate -conf /full/path/to/spot.auth

ARGUMENTS:
    help            show command documentaion
    activate        authorize the app
        --conf    full path to the auth configuration file (mandatory)

https://developer.spotify.com/documentation/
"""
import sys
import json

import src.api.operations as ops

from src.utils import fileops
from src.utils import setopts
from src.api.client import APIClient
from src.api.endpoints import endpoints

client = APIClient()
this = sys.modules[__name__]


class user(setopts.SetOpts):
    """spotify-cli 1.2.0 [2021-10-27]

USER:

    ACTIVATE:
        GET AN AUTHORIZATION CODE FOR THE APP:

        The first time you run the client, you'll be required to 
        authorize the app. Authorizing the app is done with the 
        following command:

            spotify user activate --conf /path/to/spot.auth

        The spot.auth file is a json formatted configuration file 
        containing the necessary authorization data.

        AUTH FILE:

            client:         the client id issued by Spotify for your app

            state:          csrf token string used as a url parameter to
                            prevent cross site forgery attacks. It is
                            optional and can be left out if desired.

            redirect_uri:   valid url to a domain you control used for
                            redirection. this is mandatory.

            scope:          a list of the domains within the spotify api
                            this app will have permission to access.

        Runnning the command with the appropiate config file will launch
        the default system browser, prompting spotify user login and 
        account authorization of the app. Once authorization is granted,
        the user is redirected to a webpage displaying the app's 
        authorization code.

        Copy the authcode and paste it into the command prompt. The app
        will automatically use the authcode to request an access token
        from the  Spotify API.  An access token is required to be in the
        url parameters in order to make successful calls to the api.

        This activation only needs be executed once because once an
        access token is granted, it can be refreshed indefinitely.

        The application scope can be modified to accommodate other
        endpoints by adding the new scopes to the scope list in the
        spot.auth file. However, new commands must be written accomodate
        the new endpoints and activation process must be performed again
        each time the scope is altered to grant the app the appropiate
        permissions.

    HELP
        display this documentation

See https://developer.spotify.com/documentation/general/guides/scopes
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        with open(self.options.conf, 'r') as file:
            config = json.load(file)

        app_key = config.pop('client_secret')
        encoded_auth_id = fileops.base64encode(f"{config.get('client_id')}:{app_key}")
        fileops.write(encoded_auth_id, fileops.authencoded)

        authorization_code = ops.user.authorize(config)
        print('spotify authorization code granted')
        authorized = ops.user.generate_access_token(authorization_code, encoded_auth_id)
        print('spotify access token has been cached\n')
        ops.user.set_default(authorized)
