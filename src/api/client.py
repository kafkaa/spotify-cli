import sys
from functools import partial
from urllib.parse import urlencode
from requests import Request
from requests import exceptions
from requests import Session as session

from src.utils import fileops
from src.api.endpoints import endpoints

request = dict(data=dict(grant_type="refresh_token"))
request['headers'] = {"Content-Type": "application/x-www-form-urlencoded"}


class APIClient:
    PROXY = 'https://api.thirdeye.live/auth'

    def __init__(self):
        self.get = partial(self.connect, 'get')
        self.put = partial(self.connect, 'put')
        self.post = partial(self.connect, 'post')
        self.delete = partial(self.connect, 'delete')
        self.get_authorization = partial(self.connect, 'get', oauth_connection=True)
        self.post_authorization = partial(self.connect, 'post', oauth_connection=True)

    def connect(self, method, endpoint, oauth_connection=False, **kwargs):
        """HTTP connection for authorization, access and queries"""
        with session() as connection:
            api = getattr(connection, method)
            try:
                response = api(endpoint, **kwargs)
                response.raise_for_status()
                if response.content:
                    if oauth_connection:
                        content = response.headers.get('content-type')
                        if content == 'application/json':
                            return response.json()
                        return response.url
                    return response.json()
                return 1

            except exceptions.RequestException as error:
                if response.status_code == 401:
                    return 0
                sys.exit(error)

    def refresh_access(self, token, oauth_encoded):
        """oauth: refresh access token after expiration"""
        request['data'].update(refresh_token=token)
        request["headers"]['Authorization'] = f"Basic {oauth_encoded}"

        resp = self.post_authorization(endpoints.oauth.token.geturl(), **request)
        renewed = resp.get('refresh_token')
        if not renewed:
            renewed = fileops.read(fileops.token).strip('\n')

        session = dict(refresh_token=f"{renewed}", expires=resp['expires_in'])
        session.update({"header": {"Authorization": f"Bearer {resp['access_token']}"}})

        return session

    def call(self, endpoint, method=None, proc=None, args=dict(), **kwargs):
        """send request to API and process the data returned"""
        session, basic = fileops.oauth()
        for iteration in range(2):
            resp = method(endpoint, headers=session.get('header'), params=kwargs.get('params'), data=kwargs.get('data'))
            if resp:
                try:
                    next_page, new_args = proc(resp, **args)
                    if next_page:
                        args.update(new_args)
                        self.call(next_page, method=method, proc=proc, args=args, **kwargs)
                    break

                except TypeError:
                    break

            else:
                print('session expired: requesting new access token')
                session = self.refresh_access(session['refresh_token'], basic)
                fileops.store(session, fileops.session)

    def prepare_request(self, url, **kwargs):
        session, basic = fileops.oauth()
        request['data'].update(refresh_token=session['refresh_token'])
        request["headers"]['Authorization'] = f"Basic {basic}"
        return Request('GET', url, **request)
        #return Request('GET', url, **kwargs).prepare()
