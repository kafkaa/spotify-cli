"""spotify api endpoints formatted and stored in a nested namedtuple"""
from typing import NamedTuple
from urllib.parse import urlparse

BASE = urlparse('https://api.spotify.com/')


class endpoints(NamedTuple):
    search = BASE._replace(path='/v1/search')

    class oauth(NamedTuple):
        base = urlparse('https://accounts.spotify.com/')
        token = base._replace(path='api/token')

    class track(NamedTuple):
        info = BASE._replace(path='/v1/me/player/currently-playing')
        recent = BASE._replace(path='/v1/me/player/recently-played')

    class album(NamedTuple):
        info = BASE._replace(path='/v1/albums/')
        tracks = BASE._replace(path='/v1/albums/{0}/tracks/')

    class playlist(NamedTuple):
        info = BASE._replace(path='/v1/playlists/')
        user = BASE._replace(path='/v1/me/playlists')
        create = BASE._replace(path='/v1/users/{0}/playlists')
        tracks = BASE._replace(path='/v1/playlists/{0}/tracks')

    class player(NamedTuple):
        play = BASE._replace(path='/v1/me/player/play')
        pause = BASE._replace(path='/v1/me/player/pause')
        next = BASE._replace(path='/v1/me/player/next')
        prev = BASE._replace(path='/v1/me/player/previous')
        select = BASE._replace(path='/v1/me/player')
        devices = BASE._replace(path='/v1/me/player/devices')
