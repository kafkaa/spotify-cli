"""process, cache and/or display response data returned from the spotify 
   api when adding track(s) from a track, album or playlist to another playlist"""


def track(response, track=None, playlist=None):
    try:
        print('snapshot id: ', response['snapshot_id'])
        print(f"added {track} to playlist: {playlist}")

    except (TypeError, KeyError, AttributeError) as e:
        print(f'An error has occured: {e}')

    return None, None


def album(response, album_id=None, plist_id=None):
    try:
        print('snapshot id: ', response['snapshot_id'])
        print(f"added album {album_id} to playlist {plist_id}")

    except (TypeError, KeyError, AttributeError) as e:
        print(f'An error has occured: {e}')

    return None, None


def playlist(response, plist_id=None, target_plist_id=None):
    try:
        print('snapshot id: ', response['snapshot_id'])
        print(f"added playlist {plist_id} to playlist {target_plist_id}")

    except (TypeError, KeyError, AttributeError) as e:
        print(f'An error has occured: {e}')

    return None, None
