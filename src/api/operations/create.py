"""process, cache and/or display response data returned from the spotify api 
   when creating a new playlist"""
import sys


def playlist(response):
   sys.stdout.write("%-25s\n" % (f"created:       {response['name']}"))
   sys.stdout.write("%-25s\n" % (f"id:            {response['id']}"))
   sys.stdout.write("%-25s\n" % (f"url:           {response['href']}"))
   sys.stdout.write("%-25s\n" % (f"snapshot id:   {response['snapshot_id']}"))
   return None, None
