import os
import json
import src.api.operations as ops

from src.utils import fileops
from src.api.client import APIClient
from src.api.endpoints import endpoints


client = APIClient()


def like(response, cache=None, playlist=None):
    uri = response['item']['uri']
    track = response['item']['name']

    if fileops.tmp_directory():
        for interation in range(2):
            if os.path.isfile(cache):
                if fileops.isin(cache, uri):
                    print(f'{track} is already in the current playlist')
                    return None, None

                else:
                    url = endpoints.playlist.tracks.geturl()
                    args = dict(track=uri, playlist=playlist)

                    payload = json.dumps({"uris": [uri]})
                    client.call(url.format(playlist), method=client.post, data=payload, proc=ops.add.track, args=args)
                    fileops.append(cache, f'\n{uri}')
                    return None, None

            else:
                url = endpoints.playlist.info.geturl() + playlist
                client.call(url, method=client.get, proc=ops.get.playlist, args=dict(file=cache, entire=True, verbose=False))

        return None, None


def dislike(response, cache=None, playlist=None):
    uri = response['item']['uri']
    track = response['item']['name']

    if fileops.tmp_directory():
        for interation in range(2):
            if os.path.isfile(cache):
                if not fileops.isin(cache, uri):
                    print(f'{track} is not in the current playlist')
                    return None, None

                else:
                    payload = json.dumps({"tracks": [{"uri": uri}]})
                    url = endpoints.playlist.tracks.geturl().format(playlist)
                    args = dict(file=cache, uri=uri, plist=playlist)
                    client.call(url, method=client.delete, data=payload, proc=ops.delete.track, args=args)
                    return None, None
            else:
                url = endpoints.playlist.info.geturl() + playlist
                client.call(url, method=client.get, proc=get_playlist, args=dict(file=cache, verbose=False))

        return None, None
