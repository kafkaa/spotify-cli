"""process, cache and display info about tracks, album and playlists
   from data returned from the spotify api"""
import sys
import textwrap
from functools import partial
from src.utils import fileops
from src.api.client import APIClient
from src.api.endpoints import endpoints

client = APIClient()

wrap = partial(textwrap.shorten, placeholder='...')

ALBUM_HEADER = ["NAME: ", "RELEASE DATE: ", "TOTAL TRACKS: ", "URI: ", "URL: "]
PLIST_HEADER = ["PLAYLIST: ", "OWNER: ", "TOTAL TRACKS: ", "URL: ", "SNAPSHOT ID: "]


def info(resp):
    info = resp['item']
    sys.stdout.write("%-60s %-25s\n" % (f"Track:  {info['name']}", info['uri']))
    sys.stdout.write("%-60s %-25s\n" % (f"Album:  {info['album']['name']}", info['album']['uri']))
    sys.stdout.write("%-60s %-25s\n" % (f"Artist: {info['artists'][0]['name']}", info['artists'][0]['uri']))
    return None, None


def recent(resp):
    items = resp['items']
    for item in items:
        track = item['track']
        sys.stdout.write("%-60s %-25s\n" % (wrap(f"TRACK: {track['name']}", 50), track['uri']))
        sys.stdout.write("%-60s %-25s\n" % (wrap(f"ALBUM: {track['album']['name']}", 50), track['album']['uri']))
        for artist in track['artists']:
            sys.stdout.write("%-60s %-25s\n" % (wrap(f"ARTIST: {artist['name']}", 50), artist['uri']))
        print()
    return None, None


def devices(resp):
    devices = resp['devices']
    print(devices)
    return None, None


def album(resp, file=None, mode='w', header=False, album=None, verbose=True):
    items = resp.get("items")
    paginator = resp.get("next")
    track_uris = [item['uri'] for item in items]
    track_names = [item['name'] for item in items]
    if fileops.tmp_directory():
        fileops.cache(file, track_uris, mode=mode)

    if verbose:
        if header:
            url = endpoints.album.info.geturl() + album
            client.call(url, method=client.get, proc=album_info)

        for name, uri in zip(track_names, track_uris):
            sys.stdout.write("%-60s %-25s\n" % (f"{name}", uri))

    return paginator, dict(file=file, mode='a', verbose=verbose)


def playlist(resp, file=None, mode='w', entire=False, header=False, verbose=True):
    items = next(fileops.fetch(resp, "items"))
    paginator = next(fileops.fetch(resp, "next"))
    tracks = [i for i in fileops.fetch(items, 'uri') if 'track' in i]
    albums = [i for i in fileops.fetch(items, 'uri') if 'album' in i]
    if fileops.tmp_directory():
        fileops.cache(file, tracks, mode=mode)

    if verbose:
        if header:
            playlist_info(resp)

        for item, track, album in zip(items, tracks, albums):
            artists = ', '.join([i for i in fileops.fetch(item["track"]["artists"], 'name')])
            sys.stdout.write("%-60s %-25s\n" % (wrap(f"TRACK: {item['track']['name']}", 50), track))
            sys.stdout.write("%-60s %-25s\n" % (wrap(f"ALBUM: {item['track']['album']['name']}", 50), album))
            sys.stdout.write("%-25s\n" % (wrap(f"ARTIST(s): {artists[:50]}", 50)))
            sys.stdout.write("\n")

    if entire and paginator:
        return paginator, dict(file=file, mode='a', entire=True, header=False, verbose=verbose)


def album_info(resp):
    data = [resp['name'], resp['release_date'], resp['total_tracks'], resp['uri'], resp['href']]
    for tag, value in zip(ALBUM_HEADER, data):
        print(tag, value)
    print("\nTRACKS:\n")


def playlist_info(resp):
    data = [resp["name"], resp["owner"]["display_name"], resp["tracks"]["total"]]
    data.extend([resp["owner"]["href"], resp["snapshot_id"]])
    for tag, value in zip(PLIST_HEADER, data):
        print(tag, value)
    print("\nTRACKS:\n")


def playlists(resp):
    for item in resp['items']:
        sys.stdout.write("%-30s %-25s\n" % (item['name'], item['id']))

    return None, None
