import sys
import webbrowser as browser
from urllib.parse import urlencode

from src.utils import fileops
from src.api.client import APIClient
from src.api.endpoints import endpoints


client = APIClient()


def generate_authorization_url(payload):
    payload['scope'] = ' '.join(payload.get('scope'))
    payload.update({'response_type': 'code', 'show_dialog': False})
    return f"https://accounts.spotify.com/authorize?{urlencode(payload)}"


def authorize(config):
    endpoint = generate_authorization_url(config)
    url = client.get_authorization(endpoint)
    browser.open(url)
    authcode = input("copy and paste the entire redirect url from the browser bar here: ")
    fileops.write(authcode, fileops.authorized)
    return authcode


def generate_access_token(code, b64auth):
    url = endpoints.oauth.token.geturl()
    headers = dict(Authorization=f"Basic {b64auth}")
    headers['Content-Type'] = 'application/x-www-form-urlencoded'

    payload = dict(grant_type='authorization_code', code=code)
    payload['redirect_uri'] = 'https://api.thirdeye.live'

    access = client.post_authorization(url, headers=headers, data=payload)
    session = dict(refresh_token=access['refresh_token'])
    session.update(header=dict(Authorization=f"Bearer {access['access_token']}"))

    fileops.write(access['refresh_token'], fileops.token)
    fileops.store(session, fileops.session)
    return session


def set_default(session):
    url = endpoints.playlist.user.geturl()
    response = client.get(url, headers=session['header'])
    if response:
        print('YOU MUST SELECT A DEFAULT PLAYLIST:\n')
        for item in response['items']:
            sys.stdout.write("%-30s %-25s\n" % (item['name'], item['id']))
        default = input('\nenter a playlist id from the list above: ')
        fileops.collect_and_cache(default)
        fileops.write(default, fileops.plist)
        print(f'active playlist is {default}')
