"""process and display results from spotify API catalog queries"""
import sys
import textwrap
from functools import partial

from src.utils import fileops

wrap = partial(textwrap.shorten, placeholder='...')


def track(response):
    tracks = response['tracks']['items']
    for track in tracks:
        artists = ', '.join([i for i in fileops.fetch(track["artists"], 'name')])
        sys.stdout.write("%-60s %-25s\n" % (wrap(f"TRACK: {track['name']}", 50), track['uri']))
        sys.stdout.write("%-60s %-25s\n" % (wrap(f"ALBUM: {track['album']['name']}", 50), track['album']['uri']))
        sys.stdout.write("%-25s\n" % (wrap(f"ARTIST(s): {artists}", 50)))
        sys.stdout.write('\n')

    return None, None


def album(response):
    albums = response['albums']['items']
    for album in albums:
        sys.stdout.write("%-60s %-25s\n" % (wrap(f"ALBUM: {album['name']}", 50), album['uri']))
        for artist in album["artists"]:
            sys.stdout.write("%-60s %-25s\n" % (wrap(f"ARTIST: {artist['name']}", 50), album['uri']))
        sys.stdout.write('\n')

    return None, None


def artist(response):
    artists = response['artists']['items']
    for artist in artists:
        sys.stdout.write("%-60s %-25s\n" % (wrap(f"ARTIST: {artist['name']}", 50), artist['uri']))
    return None, None


def playlist(response):
    playlists = response['playlists']['items']
    for playlist in playlists:
        sys.stdout.write("%-60s %-25s\n" % (wrap(f"NAME: {playlist['name']}", 50), playlist['uri']))
        sys.stdout.write("%-60s %-25s\n" % (f"OWNER: {playlist['owner']['display_name']}", playlist['owner']['uri']))
        sys.stdout.write("%-60s %-25s\n" % (f"TOTAL TRACKS: {playlist['tracks']['total']}", f"{playlist['href']}\n"))

    return None, None


def episode():
    pass


def show():
    pass


def all():
    pass
