"""process the api response for DELETE request"""
from src.utils import fileops


def track(response, file=None, uri=None, plist=None):
    try:
        print('snapshot id: ', response['snapshot_id'])
        print(f"removed {uri} from playlist: {plist}")
        fileops.remove(file, uri)

    except (TypeError, KeyError, AttributeError) as e:
        print(f'An error has occured: {e}')

        return None, None
