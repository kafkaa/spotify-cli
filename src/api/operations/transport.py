"""transport operations feedback"""
# from src.utils.process_manager import _reboot_status_bar
import os

from src.utils.netops import client
from src.utils.fileops import status_pid


def pause(response):
    print('transport is paused')
    return None, None


def play(response):
    print('playing selected content')
    return None, None


def next(response):
    print('playing next track')
    if os.path.isfile(status_pid):
        client('localhost', 8989, 'next')
    return None, None


def prev(response):
    print('playing the previous track')
    if os.path.isfile(status_pid):
        client('localhost', 8989, 'prev')
    return None, None


def device(response):
    print(response)
    return None, None
