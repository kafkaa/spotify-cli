import os
import sys
import shlex
import subprocess as sp

from socket import error as SocketError

from . import fileops
from . import netops


def _launch_status_bar():
    if fileops.tmp_directory():
        ps = sp.Popen(shlex.split(f'{sys.executable} {fileops.status_bar} &'))
        fileops.write(str(ps.pid), fileops.status_pid)
        print('starting TMUX status bar')
        print(f"PROCESS ID: {ps.pid}")


def _stop_status_bar():
    if os.path.isfile(fileops.status_pid):
        _shutdown_status_server()
        pid = fileops.read(fileops.status_pid)
        ps = sp.run(shlex.split(f'kill -s TERM {pid}'))
        _reset_status()
        if ps.returncode == 0:
            sp.run(shlex.split(f'rm -f {fileops.status_pid}'))
            print('stopping TMUX status bar')
            sys.exit()
        sys.exit('unable to stop status bar process')
    sys.exit("status bar process is not currently active")


def _reset_status():
    message = 'connecting to spotify...'
    fileops.write(message, fileops.read(fileops.status_location))


def _shutdown_status_server():
    try:
        netops.client('localhost', 8989, 'shutdown')

    except SocketError as e:
        print(f'Server Error: {e}')
