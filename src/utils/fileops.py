"""file input/output operations"""
import os
import sys
import json
import base64
from pathlib import Path

import src.api.operations as ops
from src.api.client import APIClient
from src.api.endpoints import endpoints

# PATHS
home = Path(sys.path[0])
access = home / '.access'
subcommands = home / 'src/subcmds'

cache_path = Path('/tmp/spotify-cli')
operations = Path('src/api/operations')

user = access / 'user'
plist = access / 'uri'
token = access / 'token'
session = access / 'session'
authorized = access / 'authcode'
authencoded = access / 'authencoded'

status_location = access / 'status.loc'
status_pid = cache_path / 'status.pid'
status_bar = home / 'src/utils/status_bar.py'


client = APIClient()

def write(data, filename):
    try:
        with open(filename, 'w') as file:
            file.write(data)

    except FileNotFoundError as error:
        print(f'write operation failed\n{error}')

    except OSError as error:
        print(f'write operation failed\n{error}')


def store(headers, filename):
    with open(filename, 'w') as file:
        json.dump(headers, file)


def read(filename):
    with open(filename) as file:
        return file.read()


def load(filename):
    with open(filename) as session:
        return json.load(session)


def oauth():
    return load(session), read(authencoded)


def tmp_directory():
    """create temp directory if it doesn't already exist"""
    if os.path.isdir(cache_path):
        return True

    try:
        os.mkdir(cache_path)
        return True

    except OSError as error:
        print(f"unable to create tmp directory: {error}")
        return False


def cache(filename, data, mode='w'):
    """create a cache file"""
    with open(filename, mode) as cache:
        cache.write('\n')
        cache.write('\n'.join(data))


def collect(cache, client, url, callback=None, args=dict()):
    if not os.path.isfile(cache):
        client.call(url, method=client.get, proc=callback, args=args)
    uris = read(cache).split("\n")
    return uris[1:100] if len(uris[1:]) > 100 else uris[1:]


def isin(cache, data):
    """lazily checks cache for data"""
    with open(cache, 'r') as cached:
        return next((True for line in cached if data in line), False)


def append(cache, data):
    """add data to the cache"""
    with open(cache, 'a') as cache:
        cache.write(f'{data}')


def remove(cache, data):
    tmp = cache_path / 'tmp.plist'
    with open(cache, 'r') as input:
        with open(tmp, 'w') as output:
            for line in input:
                if line.strip('\n') != data:
                    output.write(line)
    os.replace(tmp, cache)


def base64encode(data):
    """encode text to binary"""
    data_bytes = data.encode('ascii')
    base64_bytes = base64.urlsafe_b64encode(data_bytes)
    return base64_bytes.decode('ascii')


def fetch(iterable, token):
    """designed for parsing tortuous json files, fetch is
       a recursive generator that locates the specified json key 
       at any nested depth or complexity.

       Fetch returns a generator.

       ARGUMENTS:
           iterable: json file or object; (dict, list):

                     a json file with any number of nested lists
                     and dictionaries.

           token: str:
                    the json/dict key that points to the desired data.
    """
    if isinstance(iterable, dict):
        for key, value in iterable.items():
            if key == token:
                yield iterable[key]
            if isinstance(value, (dict, list)):
                yield from fetch(value, token)

    elif isinstance(iterable, list):
        for item in iterable:
            if isinstance(item, (dict, list)):
                yield from fetch(item, token)


def chunks(iterable, size):
    """yield successive n-sized chunks from an iterable"""
    for i in range(0, len(iterable), size):
        yield iterable[i:i + size]


def collect_and_cache(plist):
    """cache the active playlist"""
    cache = cache_path / f'{plist}.plist'
    url = endpoints.playlist.info.geturl() + plist
    if tmp_directory():
        args = dict(file=cache, entire=True, verbose=False)
        client.call(url, method=client.get, proc=ops.get.playlist, args=args)
