"""arguments are defined in the src/conf/command/sub-command.conf file. 
   for comprehensive documentation on argument wrangling using argparse, 
   please visit: https://docs.python.org/3/library/argparse.html"""
import os
import sys
import json
from argparse import ArgumentParser

from src.utils.fileops import subcommands
from src.utils.auxillaries import devnull


class SetOpts(ArgumentParser):

    @devnull
    def __init__(self, arguments):
        command = arguments.pop(0)
        super().__init__(add_help=False)
        self.sub_parser = self.add_subparsers(parser_class=ArgumentParser)

        *_, files = next(os.walk(subcommands / command))
        for file in files:
            name = file.split('.')[0]
            setattr(self, name, self.sub_parser.add_parser(name))
            sub_command = getattr(self, name)
            sub_command.set_defaults(cmd=name)
            for key, value in self.load(command, file).items():
                sub_command.add_argument(key, **value)

        try:
            self.argerror = False
            self.options = self.parse_args(arguments)

        except SystemExit:
            self.argerror = True

    def load(self, command, config):
        with open(subcommands / command / config, 'r') as file:
            opts = json.load(file)
            for value in opts.values():
                if 'type' in value:
                    value['type'] = type(value['type'])
        return opts

    def validate(self, message):
        if self.argerror or 'cmd' not in self.options:
            sys.exit(message.strip('\n'))

    def docs(self, documentation):
        if self.options.cmd == 'help':
            print(documentation.strip('\n'))
            sys.exit(0)
