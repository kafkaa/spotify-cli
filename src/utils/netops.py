import socket
import socketserver


def status_server(host, port, execute):
    class StatusHandler(socketserver.BaseRequestHandler):
        def handle(self):
            self.data = self.request.recv(1024).strip()
            if self.data == b'shutdown':
                self.server._BaseServer__shutdown_request = True

            elif self.data in (b'next', b'prev', b'play', b'pause'):
                execute()

    with socketserver.TCPServer((host, port), StatusHandler) as server:
        server.serve_forever()


def client(host, port, message):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        s.send(bytes(message.encode()))
