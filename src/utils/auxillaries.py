"""auxillary help functions and data"""
import os
import contextlib


def devnull(func):
    """redirect stderr to /dev/null"""
    def nullified(*args, **kwargs):
        with open(os.devnull, 'w') as null:
            with contextlib.redirect_stderr(null):
                func(*args, **kwargs)
    return nullified
