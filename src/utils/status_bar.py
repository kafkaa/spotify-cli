import sys
import json
import threading
from time import sleep
from textwrap import shorten

sys.path.insert(0, sys.path[0][:43]) # <- come up with a more flexible solution (if not using docker)

import fileops
from netops import status_server
from src.api.client import APIClient
from src.api.endpoints import endpoints


class StatusBar:
    TAGS = ['ARTIST: ', 'ALBUM: ', 'TRACK: ']

    def __init__(self, file, offset=0):
        self.file = file
        self.remaining = 0
        self.offset = offset
        self.client = APIClient()
        self.exit = threading.Event()

    def update(self, status):
        fileops.write(status, self.file)

    def poll(self):
        url = endpoints.track.info.geturl()
        self.client.call(url, method=self.client.get, proc=self.process)

    def process(self, resp):
        def get_time_remaining(resp):
            duration = next(fileops.fetch(resp, 'duration_ms'))
            position = next(fileops.fetch(resp, 'progress_ms'))
            return duration - position

        if resp.get('is_playing'):
            self.remaining = get_time_remaining(resp)
            info = list({value: 0 for value in fileops.fetch(resp, 'name')})
            text = list(zip(self.TAGS, info))
            display = ' | '.join([''.join(item) for item in text])
            self.update(shorten(display, width=100, placeholder='...'))
        else:
            self.update('spotify transport state is currently inactive')

        return None, None

    def run(self):
        while True:
            while not self.exit.is_set():
                self.poll()
                self.exit.wait((self.remaining / 1000.0) + self.offset)

            self.exit.clear()

    def restart(self):
        self.exit.set()


if __name__ == '__main__':
    status = StatusBar(fileops.read(fileops.status_location))
    srv = threading.Thread(daemon=True, target=status_server, args=('localhost', 8989, status.restart))
    srv.start()
    status.run()
