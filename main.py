#!/Users/kafka/.local/python/env/apps/spotify/.venv/bin/python3
"""spotify-cli 1.2.0 [2021-10-27]

The spotify-cli is used to access and manage an authorized user's account.

USAGE:
    spotify [command] [subcommand] [--options]

COMMANDS:
    add         : add track, album or playlist to a specified playlist
    create      : create a new objects, such as a playlist
    get         : fetch data from the api
    search      : search for an artist, album, track or playlist
    set         : set application paramters
    status      : displays info in the TMUX status bar if running and properly configured
    track       : perform operations on track data
    transport   : spotify player transport controls
    user        : account authorization and managment

for comprehensive documentation of each command, use:
    spotify <command> help

more information @https://developer.spotify.com/documentation/web-api/
"""
import sys
import time
import importlib

this = sys.modules[__name__]

if __name__ == '__main__':

    start = time.time()

    try:
        command = sys.argv[1]
        module = importlib.import_module(f'src.cmds.{command}')
        command = getattr(module, command)
        command = command(sys.argv[1:])
        command.execute()

    except (IndexError, ModuleNotFoundError) as e:
        print(f"ERROR MAIN: {e}")
        print(this.__doc__)

    print(f"finished in {time.time() - start} seconds")
